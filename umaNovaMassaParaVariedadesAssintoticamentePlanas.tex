\documentclass[12pt,a4paper,draft,twoside]{report}
\usepackage[utf8]{inputenc}
\usepackage[portuguese]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage[showlabels,sections,floats,textmath,displaymath,psfixbb]{preview}

\theoremstyle{plain}\newtheorem{definicao}{Definição}[chapter]
\newbox\chaos
\newdimen\tdim
\def\fframe{%
\tdim=\columnwidth
\advance\tdim by -2\fboxsep
\advance\tdim by -2\fboxrule
\setbox\chaos=\hbox\bgroup\begin{minipage}{\tdim}}
\def\endfframe{\end{minipage}\egroup\fbox{\box\chaos}}
\unitlength 1mm
\newcount\fives
\fives 10
\newcount\ones
\ones\fives
\multiply \ones by 10
\newsavebox{\raster}
\savebox{\raster}(\ones,\ones)
{\thicklines
  \put(0,0){\line(0,1){\ones}}
  \put(0,0){\line(1,0){\ones}}
  \multiput(0,0)(5,0){\fives}
  {\begin{picture}(0,0)
      \put(5,0){\line(0,1){\ones}}
      \thinlines\multiput(1,0)(1,0){4}{\line(0,1){\ones}}
    \end{picture}}
  \multiput(0,0)(0,5){\fives}
  {\begin{picture}(0,0)
      \put(0,5){\line(1,0){\ones}}
      \thinlines\multiput(0,1)(0,1){4}{\line(1,0){\ones}}
    \end{picture}}
}
\author{Alexandre "asm"}
\title{Uma nova massa para variedades assintoticamente planas}
\begin{document}

\chapter{Curvaturas de Lovelock e Gauss-Bonnet}

O tensor de Einstein,
\begin{displaymath}
  E = Ric - \frac{1}{2} R g,
\end{displaymath}
de uma variedade Riemanniana $(M,g)$ é muito importante em física no estudo da teoria da relatividade geral, e certamente também em matemática. Ele admite uma propriedade
notável, à saber, é um tensor \emph{livre de divergência}, i.e.,
\begin{equation*}
  \label{eq:EinLivreDiv}
  div\ E = 0,
\end{equation*}
onde $div$ é a divergência calculada com respeito a conexão de Levi-Civita determinada pela métrica $g$; no jargão físico essa propriedade se traduz dizendo que esse
tensor é uma \emph{quantidade conservada}. Na tentativa de generalizar essa quantidade geométrica, Lovelock\cite{Lo} e Lanczos \cite{Lan} estudaram a classificação dos
tensores $A$ satisfazendo as seguintes propriedades:
\begin{itemize}
\item [(i)] $A^{ij} = A^{ji}$, i.e., $A$ simétrico de posto $2$;
\item [(ii)] $A^{ij} = A^{ij}(g_{ab}, g_{ab,c}, g_{ab,cd}$, $A$ depende da métrica e suas derivadas até de segunda ordem;
\item [(iii)] $\nabla_j A^{ij}=0$, i.e., $A$ é livre de divergência;
\item [(iv)] $A$ é linear nas derivadas de segunda ordem da métrica.
\end{itemize}
Verifica-se que o tensor de Einstein satisfaz todas as propriedades. De fato, o tensor de Einstein é único tensor satisfazendo todas as quatro propriedades, a menos de um
múltiplo constante. Lovelock classificou todos os tensores satisfazendo as propriedades (i), (ii) e (iii); ele provou que qualquer tensor que satisfaz essas três
propriedades é uma combinação linear
\[\sum_{k}\alpha_k \ ^{(k)}E,\]
com coeficientes constantes $\alpha_k$, $k \ge 0$, onde os tensores $^{(k)}E$, chamados \emph{k-ésimo tensores de Lovelock} ou \emph{k-ésima curvaturas de Lovelock}, são definidos por

\begin{equation}
  ^{(k)}E := - \frac{1}{2^{2k+1}\ (2k+1)!} (C_g)_{(2,2k+3)(3,2k+4)\cdots (2k,4k+1)(2k+1,4k+2)} (g\wedge R\wedge \stackrel{k}{\ldots} \wedge R ),
  \label{eq:curvLov}
\end{equation}
onde $g$ e $R$ denotam a métrica e o tensor de curvatura de Riemmann, respectivativamente; note que $g \in \Lambda^1 T^{\ast}M \bigotimes \Lambda^1T^{\ast}M$
e $R \in \Lambda^2 T^{\ast}M \bigotimes \Lambda^2 T^{\ast}M$.
Convenciona-se que $^{(0)}E=1$, e um cálculo direto mostra que $^{(1)}E$ é o tensor de Einstein. Dessa forma, as k-ésimas curvaturas de Lovelock $^{(k)}E$ são generalizações naturais
do tensor de Einstein. Fixados um referencial local $\{e_i\}_{i=1}^{dim\ M}$ e o seu co-referencial local $\{\theta^i\}_{i=0}^{dim\ M}$ correspondente, as k-ésimas curvaturas de Lovelock,
reescritas na linguagem de componentes, tomam a seguinte forma

\begin{equation}
    ^{(k)}E_{ij} =-\frac{1}{2^{k+1}}g_{li}\delta^{li_1i_2\cdots i_{2k-1}i_{2k}}_{jj_1j_2\cdots j_{2k-1}i_{2k}}{R_{i_1i_2}}^{j_1j_2}\cdots{R_{i_{2k-1}i_{2k}}}^{j_{2k-1}j_{2k}},
    \label{eq:curvLovComp}
\end{equation}
onde ${R_{i_ji_l}}^{j_mj_n}$ são as componentes do tensor do tipo $(2,2)$ metricamente quivalente ao tensor curvatura de de Riemann, e $\delta^{j_1j_2 \dots j_r}_{i_1i_2 \dots i_r}$ é a \emph{delta de Kronecker generalizada}
que é definida por
\begin{equation}
    \delta^{j_1j_2 \dots j_r}_{i_1i_2 \dots i_r}=\det\left(
    \begin{array}{cccc}
    \delta^{j_1}_{i_1} & \delta^{j_2}_{i_1} &\cdots &  \delta^{j_r}_{i_1}\\
    \delta^{j_1}_{i_2} & \delta^{j_2}_{i_2} &\cdots &  \delta^{j_r}_{i_2}\\
    \vdots & \vdots & \vdots & \vdots \\
    \delta^{j_1}_{i_r} & \delta^{j_2}_{i_r} &\cdots &  \delta^{j_r}_{i_r}
    \end{array}
    \right).
    \label{eq:deltaKronGen}
\end{equation}
Tomando o traço, a menos de um múltiplo constante, obtemos \emph{k-ésima curvatura de Gauss-Bonnet}
\begin{equation}
  \label{eq:curvGaussBonnet}
  L_k := \frac{(-1)^{k+1}}{2^{2k}\ (2k)!} (C_g)_{(1,2k+1)(2,2k+2)\cdots (2k-1,4k-1)(2k,4k)}(R \wedge \stackrel{k}{\cdots} \wedge R),
\end{equation}
que na linguagem de componentes pode ser reescrita como
\begin{equation}
  \label{eq:curvGaussBonnetComp}
  L_k:=\frac{{(-1)^{k+1}}}{2^k}\delta^{i_1i_2\cdots i_{2k-1}i_{2k}}_{j_1j_2\cdots j_{2k-1}j_{2k}}{R_{i_1i_2}}^{j_1j_2}\cdots {R_{i_{2k-1}i_{2k}}}^{j_{2k-1}j_{2k}}.
\end{equation}
Ambas tem sido estudadas extensivamente no contexto da gravidade de Gauss-Bonnet, que é uma generalização da gravidade de Einstein. Haja visto que
$R\wedge \stackrel{k}{\cdots} \wedge R \in \Lambda^{2k}(T^{\ast}M) \bigotimes \Lambda^{2k}(T^{\ast}M),$ segue que $L_k= 0$ quando $2k > dim\ M$. Quando $2k = dim\ M$, $L_k$ é de fato a densidade de Euler, que foi estudada por Chern \cite{Chern1, Chern2}
em sua prova do teorema de Gauss-Bonnet-Chern.

Uma propriedade fundamental em muitas aplicações das k-ésimas curvaturas de Gauss-Bonnet, em particukar a que faremos a seguir, é o fato de que tais curvaturas admitem
a seguinte decomposição
\begin{equation}
  \label{eq:curvGaussBonnetDecompComp}
  L_k =\frac{{(-1)^{k+1}}}{2^k} R^{mnj_1 j_2} g_{m i_1}g_{n i_2} \delta^{i_1i_2i_3i_4\cdots i_{2k-1}i_{2k}}_{j_1j_2j_3j_4\cdots j_{2k-1}j_{2k}}{R_{i_3i_4}}^{j_3j_4}\cdots {R_{i_{2k-1}i_{2k}}}^{j_{2k-1}j_{2k}}
  ={{(-1)^{k+1}}} R^{mnj_1 j_2} \ ^{(k)}P_{mnj_1j_2},
\end{equation}
onde $^{(k)}P_{mnpq}$ são as componentes do tensor $(0,4)$ dadas por
\begin{equation}
  \label{eq:curvPcomp}
  ^{(k)}P_{mnpq} :=\frac{1}{2^k} g_{ms}g_{nt} \delta^{sti_1i_2\cdots i_{2k-3}i_{2k-2}}_{pqj_1j_2\cdots j_{2k-3}j_{2k_2}}{R_{i_1i_2}}^{j_1j_2}\cdots {R_{i_{2k-3}i_{2k-2}}}^{j_{2k-3}j_{2k-2}},
\end{equation}
que pode ser reescrito da seguinte forma
\begin{equation}
  \label{eq:curvP}
  ^{(k)}P := \frac{1}{2^{2k} (2k)!} (C_g)_{(3,2k+3)(4,2k+4)\cdots (2k-1,4k-1)(2k,4k)}(g\wedge g\wedge R\wedge \stackrel{k-1}{\ldots} \wedge R).
\end{equation}
Haja visto que a conexão de Levi-Civita $\nabla$ é compatível com a métrica, i.e.,
\begin{equation}
  \label{eq:compMet}
  d_{\nabla}g = 0,
\end{equation}
e o tensor de curvatura de Riemann satisfaz a identidade diferencial de Bianchi,i.e.,
\begin{equation}
  \label{eq:identDifBianchi}
  d_{\nabla}R = 0,
\end{equation}
segue que o tensores $^{(k)}P$ também satifazem a identidade diferencial de Bianchi
\begin{equation}
  \label{eq:identDifBianchiP}
  d_{\nabla}{}^{(k)}P = 0.
\end{equation}
Haja visto que o produto exterior de uma $p$-forma a valores em $\Lambda^p(T^{\ast}M)$ com uma $q$-forma a valores em $\Lambda^q(T^{\ast}M)$, ambas simétricas, resulta
em uma $(p+q)$-forma a valores em $\Lambda^{p+q}(T^{\ast}M)$ simétrica, segue que $^{(k)}P$ também é simétrico, i.e.,
\begin{equation}
  \label{eq:simetriaP}
  \langle ^{(k(}P(X \wedge Y), Z \wedge W \rangle = \langle ^{(k(}P(W \wedge Z), X \wedge Y \rangle, \quad \forall \ X,Y,Z,W \in \mathfrak{X}(M).
\end{equation}
Usando essa propriedade de simetria obtemos seguintes propriedades de antisimetria para os tensores $^{(k)}P$,
\begin{eqnarray}
  \label{eq:antiSimP}
  \langle ^{(k(}P(X \wedge Y), Z \wedge W \rangle &=& - \langle ^{(k(}P(Y \wedge W), Z \wedge X \rangle \\
  &=& - \langle ^{(k(}P(X \wedge Y), Z \wedge W \rangle,  \quad \forall \ X,Y,Z,W \in \mathfrak{X}(M).
\end{eqnarray}
Usando essas propriedades, a equação de compatibilidade da métrica e a identidade de diferencial de Bianchi para o tensor de Riemann, temos
'\begin{eqnarray*}
   \nabla^m\ ^{(k)}P_{mnpq} &=& -\nabla^m\ ^{(k)}P_{nmpq}= -\nabla^m\ ^{(k)}P_{mnqp}= \nabla^m\ ^{(k)}P_{pqmn}\\
  &=& g^{mr} \nabla_r \left( \frac{1}{2^k}\sum_{\sigma} sgn(\sigma) g_{m\sigma(p)}g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
    {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}} \right) \\
  &=&\frac{2}{2^k} g^{mr} \sum_{\sigma} sgn(\sigma) \overbrace{\nabla_{r}g_{m\sigma(p)}}^{=0}\ g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
  {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}} \\
  & & +\frac{1}{2^k} \sum_{i=1}^{k-1} \sum_{\sigma} sgn(\sigma)g^{mr} g_{m\sigma(p)}g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
  \nabla_{r}{R_{\sigma(j_{2i-1})\sigma(j_{2i})}}^{j_{2i-1}j_{2i}}\cdots {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}} \\
  &=& \frac{1}{2^k} \sum_{i=1}^{k-1} \sum_{\sigma} sgn(\sigma) g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
  \nabla_{\sigma(p)}{R_{\sigma(j_{2i-1})\sigma(j_{2i})}}^{j_{2i-1}j_{2i}}\cdots {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}} \\
  &=& \frac{1}{3\ 2^k} \sum_{i=1}^{k-1} \sum_{\sigma} sgn(\sigma) g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
  \left( \nabla_{\sigma(p)}{R_{\sigma(j_{2i-1})\sigma(j_{2i})}}^{j_{2i-1}j_{2i}} + \nabla_{\sigma(j_{2i-1})}{R_{\sigma(j_{2i})\sigma(p)}}^{j_{2i-1}j_{2i}}
  + \nabla_{\sigma(j_{2i})}{R_{\sigma(p)\sigma(j_{2i-1})}}^{j_{2i-1}j_{2i}} \right)\cdots {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}} \\
  &=& \frac{1}{3\ 2^k} \sum_{i=1}^{k-1} \sum_{\sigma} sgn(\sigma) g_{n\sigma(q)}{R_{\sigma(j_1)\sigma(j_2)}}^{j_1j_2}\cdots
  \overbrace{{(d_{\nabla}R)_{\sigma(p)\sigma(j_{2i-1})\sigma(j_{2i})}}^{j_{2i-1}j_{2i}}}^{=0}\cdots {R_{\sigma(j_{2k-3})\sigma(j_{2k-3})}}^{j_{2k-3}j_{2k-2}}\\
  &=& 0,
\end{eqnarray*}
ou seja, os \emph{tensores $^{(k)}P$ são livres de divergência}\label{livredivP}. Essa propriedade já havia sido notada na literatura física, por exemplo, no trabalho
de S. Davis\cite{Davis}.

\chapter{A massa de Gauss-Bonnet-Chern}

Por ora nos concentraremos no caso $2$. Os resultados podem ser generalizados para $k < \frac{dim\ M}{2}$, conforme o exposto no capítulo \cite{cap:generalizacoes}.

Paasaremos agora a descrever uma nova definição de massa, introduzida por Yuxin Ge, Guofang Wang e Jie Wu usando a curvatura de Gauss-Bonnet para variedades 
assintoticamente planas \cite{GeWangWu} com ordem de decaimentos apropriada. Isso é comparável a definição de massa ADM, que pode ser definida usando a curvatura
escalar, conforme indicado na introdução do trabalho dos autores supracitados. Ademais, seguindo as ideias do Robert Bartnik\cite{Bar} (veja também\cite{LeePar}),
prova-se que essa nova massa é um invariante geométrico, ou seja, não depende da escolha de coordenada no infinito.

Uma variedade Riemanniana $(M^n,g)$ completa é dita ser assintoticamente plana de ordem $\tau$ (com um fim), se admite um subconjunto compacto $K \subset M$ e um difeomosfismo
$\Phi: M\setminus K \longrightarrow \mathbb{R}^n\setminus B_R(0)$, para algum ${R > 0}$, tal que nas coordenadas induzidas pelas coordenadas canônicas do $\mathbb{R}^n$ (via o difeomosfismo), a métrica $g$ tem a seguinte
expansão\label{def:assPla}
$$g_{ij}=\delta_{ij}+\sigma_{ij},$$
$$\text{ com }\quad |\sigma_{ij}|+ r|\partial\sigma_{ij}|+ r^2|\partial^2\sigma_{ij}|=O(r^{-\tau})$$

para algum $\tau \in \mathbb{R}$ positivo, onde $r$ e $\partial$ denotam a distâcia Euclideana e o operador derivada canônicos do $\mathbb{R}^n$ com a métrica Euclidiana $\delta$, respectivativamente.
Para tais variedades, a (segunda) curvatura de Gauss-Bonnet(\cite{curvGaussBonnet}) pode ser expressa como um termo de divergência, a menos de alguns termos com
taxas de decaimento mais rápidas. Para isso, lembremos que, em coordenadas locais, o tensor de curvatura pode ser expresso como
$$R_{ijk}^{m}=\partial_i \Gamma^m_{jk}-\partial_j \Gamma^m_{ik}+\Gamma^m_{is}\Gamma^s_{jk}-\Gamma^m_{js}\Gamma^s_{ik},$$
onde $\Gamma^m_{jk}$ são os símbolos de Christoffel, definidos por
$$\Gamma^m_{jk} :=\frac{1}{2}g^{ml}\left( \partial_jg_{kl} +\partial_kg_{jl} -\partial_lg_{jk} \right).$$
Haja visto que a matriz da métrica $g_{ij}$ e sua inversa $g^{ij}$ satisfazem a relação
$$g_{im}g^{mj} = \delta_i^j,$$
da expansão da métrica\cite{def:asspla} segue que
$$g^{ij}=\delta_{ij} -g^{il}\sigma_{lj}= \delta_{ij} -\sigma_{ij} + g^{ik}\sigma_{kl}\sigma_{lj}= \delta_{ij} +\sum_{a=1}^{\infty}(-\sigma)^a_{ij},$$
ou seja, a matriz inversa da métrica admite a mesma expansão da métrica
$$g^{ij}= \delta_{ij} + \theta_{ij}$$
$$\text{ com }\quad |\theta_{ij}|+ r|\partial\theta_{ij}|+ r^2|\partial^2\theta_{ij}|=O(r^{-\tau}).$$
Dessas expansões obtemos que
$$\Gamma^m_{jk} = \frac{1}{2}\left( \partial_j\sigma_{km} +\partial_k\sigma_{jm} -\partial_m\sigma_{jk} \right)
+\frac{1}{2}\theta^{ml}\left( \partial_j\sigma_{kl} +\partial_k\sigma_{jl} -\partial_l\sigma_{jk} \right)= O(r^{-\tau-1})$$
e
$$\partial_i\Gamma^m_{jk} =\frac{1}{2}\partial_i\theta_{ml}\left( \partial_j\sigma_{kl} +\partial_k\sigma_{jl} -\partial_l\sigma_{jk} \right)
+\frac{1}{2}(\delta_{ml} +\theta^{ml})\left( \partial^2_{ij}\sigma_{kl} +\partial^2_{ik}\sigma_{jl} -\partial^2_{il}\sigma_{jk} \right) =O(r^{-\tau-2}).$$
Logo, os termos do tensor de curvatura que são quadráticos nos símbolos de Christoffel tem taxas de decaimento maiores que as das derivadas desses símbolos. Assim, o
tensor de curvatura admite a seguinte expansão
$$R^m_{ijk} =\partial_i \Gamma^m_{jk}-\partial_j \Gamma^m_{ik} + O(r^{-2\tau-2}) = O(r^{-\tau-2}).$$
Donde obtemos que
$${R_{ij}}^{km} = g^{kl}R^m_{ijl}=(\delta_{kl} +\theta_{kl})R^m_{ijl}=O(r^{-\tau-2}),$$
e por consequência
$$P_{ijkm}= O(r^{-\tau-2}),$$
onde $P$ denota o tensor $^{(2)}P$ obtido na decomposição da curvatura de Gauss-Bonnet $L_2$\cite{eq:curvP}. Usando o fato de que o tensor $P$ é livre de
divergência\cite{livredivP} e as suas simetrias\cite{eq:antiSimP}, temos
\begin{eqnarray}
  \label{eq:curvGBdiv}
  L_2&=&-R_{ijlk}P^{ijlk}=g_{km}R^m_{ijl}P^{ijkl}\nonumber\\
  &=&g_{km}(\partial_i \Gamma^m_{jl} -\partial_j \Gamma^m_{il})P^{ijkl}+O(r^{-4-3\tau})\nonumber\\
  &=&g_{km}\bigg[\nabla_i(\Gamma^m_{jl}P^{ijkl}) -\Gamma^m_{jl}\underbrace{\nabla_iP^{ijkl}}_{=0}
  -\nabla_j(\Gamma^m_{il}P^{ijkl}) +\Gamma^m_{il} \underbrace{\nabla_jP^{ijkl}}_{=0} \bigg]+O(r^{-4-3\tau})\nonumber\\
  &=&\frac 12\nabla_i\bigg[(g_{jk,l}+g_{kl,j}-g_{jl,k})P^{ijkl}\bigg]-\frac 12\nabla_j\bigg[(g_{ik,l}+g_{kl,i}-g_{il,k})P^{ijkl}\bigg]+O(r^{-4-3\tau})\nonumber\\
  &=&2\nabla_i\bigg(g_{jk,l}P^{ijkl}\bigg)+O(r^{-4-3\tau})\nonumber\\
  &=&2\partial_i\bigg(g_{jk,l}P^{ijkl}\bigg) + \sum (\partial g\cdot P \cdot \Gamma)_{ijkl} +O(r^{-4-3\tau}) \nonumber\\
  &=&2\partial_i\bigg(g_{jk,l}P^{ijkl}\bigg) +O(r^{-4-3\tau}),
\end{eqnarray}
onde $\sum (\partial g\cdot P \cdot \Gamma)_{ijkl} =O(r^{-5-3\tau})$. Tomemos uma exaustão $\{B_r\}_{r > R}$ da subvariedade $M\setminus K$ por compactos $B_r :=\Phi^{-1}(B_r(0)\setminus B_R(0)) \subset M$, com $B_r(0) \subset \mathbb{R}^n$, de modo que
$S_r :=\partial B_r = \Phi^{-1}(S_r(0))$, com $S_r(0)= \partial B_r(0) \subset \mathbb{R}^n$, ou seja, uma exaustão por bolas coordenadas centradas na origem. Assim, integrando a expressão para a divergência da
curvatura de Gauss-Bonnet\cite{eq:curvGBdiv} sobre alguma bola $B_r$ com raio $r$ suficientemente grande, e aplicando a desigualdade triangular e o teorema da divergência,
obtemos
\begin{eqnarray*}
  & & \bigg| \int_{B_r} L_2 -\int_{B_r}\partial_i\bigg(g_{jk,l}P^{ijkl}\bigg) \bigg| \le C \int_{B_r} r^{-4-3\tau} \\
  &\Longrightarrow& \bigg| \int_{B_r} L_2 -\int_{S_r}P^{ijkl}\partial_lg_{jk} \nu_i \bigg| \le \frac{C(n)}{n-3-4\tau} \bigg( r^{-4-3\tau +n} -R^{-3-4\tau +n} \bigg),
\end{eqnarray*}
onde $C$ e $C(n)$ são constantes positivas. Passando ao limite ao limite quando $r \to \infty$ e $n-4 -3\tau <0$, segue que
\begin{equation}
  \label{eq:curvGBint}
  \bigg| \int_{M\setminus K} L_2 -\lim_{r \to \infty}\int_{S_r}P^{ijkl}\partial_lg_{jk} \nu_i \bigg| \le -\frac{C(n)}{n-3-4\tau}R^{-3-4\tau +n}.
\end{equation}
Logo, se a curvatura de Gauss-Bonnet é integrável em $(M^n,g)$, ou seja, se $L_2 \in L^1(M,g)$, então
\begin{equation}
  \bigg| \lim_{r \to \infty}\int_{S_r}P^{ijkl}\partial_lg_{jk} \nu_i \bigg| \le\bigg| \int_{M\setminus K} L_2 \bigg| -\frac{C(n)}{n-3-4\tau}R^{-3-4\tau +n} \ < \infty,
\end{equation}
ou seja, esse limite existe e é finito. Posto isto, definimos a massa de Gauss-Bonnet-Chern
\begin{definicao}[Massa de Gauss-Bonnet-Chern]
  \label{def:massaGBC}
  Seja $(M^n,g)$ uma variedade assintoticamente plana de ordem $\tau > \frac{n -4}{3}$, com dimensão $n \ge 4$, e (segunda) curvatura de Gauss-Bonnet-Chern $L_2$
  inetgrável sobre $(M^n,g)$. Nesse ambiente definimos a massa de Gauss-Bonnet-Chern
  \begin{equation}
    \label{eq:massaGBC}
    m_2(g):=m_{GBC}(g)=c_2(n)\lim_{r\rightarrow\infty}\int_{S_r}P^{ijkl}\partial_l g_{jk} \nu_{i}dS,
  \end{equation}
  onde
  $$c_2(n)=\frac 1 {2(n-1)(n-2)(n-3)\omega_{n-1}},$$
  é uma contante de normalização calculada utilizando as métricas de {Schwarzschild} na gravidade de Einstein-Gauss-Bonnet, $\nu$ é o campo normal exterior unitário ao
  longo de $S_r \subset M$, $dS$ é o elemento de área de $S_r$ e o tensor $P$ é dado por
  $$P =\frac{1}{2^{4} (4)!} (C_g)_{(3,4+3)(4,4+4)}(g\wedge g\wedge R) \quad
  \Longleftrightarrow \quad P^{ijkl} = \frac{1}{2^4} g^{im}g^{jn}\delta^{klq_1q_2}_{mnp_1p_2} {R_{q_1q_2}}^{p_1p_2}.$$
\end{definicao} 

Note que embora a massa esteja definida para dimensão $n=4$, nessa dimensão essa massa é sempre nula, pois
$(g\wedge g\wedge R) \in \Lambda^4(T^{\ast}M) \bigotimes \Lambda^4(T^{\ast}M)$, equanto que $dim \ S_r = 3$ para todo $r$. Logo, tomando o pulback do tensor $P$ concluímos que as integrais acima são
identicamente nulas. E mais, haja visto que $P^{ijkl}=O(r^{-3-2\tau})$, para $r$ suficientemente grande, temos
$$\bigg| \int_{S_r}P^{ijkl}\partial_l g_{jk} \nu_{i}dS\bigg| \le C \int_{S_r}r^{-3-2\tau} =C(n)r^{n-4-2\tau},$$
onde $C$ e $C(n)$ são constantes positivas. Logo, quando $n-4-2\tau <0$, obtemos
$$\bigg| \lim_{r\rightarrow\infty}\int_{S_r}P^{ijkl}\partial_l g_{jk} \nu_{i}dS \bigg|=0,$$
ou seja, quando a taxa de decaimento $\tau > \frac{n-4}{2}$ a massa de Gauss-Bonnet-Chern é sempre nula. Assim, a métrica usual de Schwarzschild
$$g_{\rm Sch}^{(1)}=\bigg(1-\frac{2m}{\rho^{n-2}}\bigg)^{-1} d\rho^2+\rho^2d\Theta^2=\bigg(1+\frac {m}{2r^{n-2}}\bigg)^{\frac {4}{n-2} }g_{{\mathbb R}^n},$$
que de acordo com o teorema de Birkoff, são as únicas soluções estáticas e rotacionalmente simétricas da equação de Einstein para o vácuo,
na teoria de gravidade de Einstein, tem massa de Gauss-Bonnet-Chern definida e sempre nula sempre que a dimensão $n \ge 4$, pois tem taxa de decaimento
${\tau =n-2 >\frac{n-4}{2} > \frac{n-4}{3}}$. Aqui $d\Theta^2$ denota a métrica canônica sobre a esfera ${\mathbb S}^{n-1}$.

Por outro lado, a seguinte métrica de Schwarzschild generalizada
$$g_{\rm Sch}^{(2)}=\bigg(1-\frac{2m}{\rho^{\frac n2-2}}\bigg)^{-1} d\rho^2+\rho^2d\Theta^2
=\bigg(1+\frac {{m}}{2r^{\frac{n-4}2}}\bigg)^{\frac {8}{n-4} }g_{{\mathbb R}^n},$$
que desempenha o papel da métrica de Schwarzschild usual na gravidade de Gauss-Bonnet (pura)\cite{CTZ}, tem massa de Gauss-Bonnet-Chern definida sempre que
a dimensão $n \ge 4$, pois ela tem taxa de decaimento $\tau =\frac{n-4}{2} > \frac{n-4}{3}$. Ademais, pode-se checar que $m_2\left( g_{sch}^{(2)} \right) =m^2$ (cap. \cite{cap:sch}),
que é não-negativa. Para discussões sobre esse tipo de métrica e outras métricas tipo Schwarzschild mais gerais, veja o capítulo \cite{cap:schGen}.


\chapter{Métricas de Schwarzschild generalizadas}
\label{cap:schGen}


\chapter{Generalizações}
\label{cap:generalizacoes}
\begin{thebibliography}{99}

\bibitem{Lan}C.  Lanczos, {\it A remarkable property of the Riemann-Christoffel tensor in four dimensions,}  Ann. of Math. (2) {\bf 39}  (1938),  no. 4, 842--850.
\bibitem{Lo} D. Lovelock, {\it The Einstein tensor and its generalizations,} J. Math. Phys. {\bf 12} (1971) 498--501.
\bibitem{Chern1} S. S. Chern, {\it  A simple intrinsic proof of the Gauss-Bonnet formula for closed Riemannian manifolds,}
  Ann. of Math. (2)  {\bf 45}  (1944), 747--752.

\bibitem{Chern2} S. S. Chern, {\it  On the curvatura integra in a Riemannian manifold,}   Ann. of Math. (2)  {\bf 46}  (1945), 674--684.
\bibitem{Davis} S. Davis, {\it Generalized Israel junction conditions for a Gauss-Bonnet brane world,}
Phys. Rev. D \textbf{67} (2003) 024030.

\bibitem{GeWangWu} Yuxin Ge, Guofang Wang, Jie Wu, {\it A new mass for asymptotically flat manifolds}, {\bf arXiv:1211.3645v2}, (2013).
\bibitem{Bar} R. Bartnik, {\it The mass of an asymptotically flat manifold,} Comm. Pure Appl. Math., {\bf 34} (1986) 661--693.
\bibitem{LeePar} J. Lee and T. Parker, {\it The Yamabe problem}, Bull. Am. Math. Soc., \textbf{17} (1987), 37--91.
\bibitem{CTZ}  J. Cris\'{o}stomo, R. Troncoso and J. Zanelli, {\it Black hole scan,} Phys. Rev. D, (3) {\bf 62} (2000), no.8, 084013.

\end{thebibliography}


\end{document}